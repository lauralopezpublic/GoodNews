//
//  NewsListTableViewController.swift
//  GoodNews
//
//  Created by Laura López on 7/2/22.
//

import Foundation
import UIKit

class NewsListTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    private func setup() {
        navigationController?.navigationBar.prefersLargeTitles = true

        guard let url = URL(string: "https://newsapi.org/v2/top-headlines?country=us&apiKey=26d36057405341b79ae475ec588a8b38") else {
            return
        }
        WebService().getArticles(url: url) { _ in

        }
    }
}
