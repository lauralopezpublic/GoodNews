//
//  WebService.swift
//  GoodNews
//
//  Created by Laura López on 7/2/22.
//

import Foundation

class WebService {
    func getArticles(url: URL, completion: @escaping(Any?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error)
                completion(nil)
            } else if let data = data {
                completion(data)
            }
        }.resume()
    }
}
